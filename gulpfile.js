"use strict";

const gulp = require("gulp");


const settings = require("./settings.json");

let production = false;

function lazyRequireTask( taskName, path, options ) {
  options = options || { };
  options.taskName = taskName;

  gulp.task(taskName, function ( callback ) {
    let task = require(path).call(this, options, production);

    return task(callback);
  });
}


lazyRequireTask("build-html", "./gulp-tasks/build-html.js", {
  src: settings.app_dir + "/jade/index.jade",
  dest: settings.app_dir,
  newer: settings.app_dir,
  jade: {
    pretty: settings.jade_prettify
  },
  jade_inheritance: {
    basedir: settings.app_dir + "/jade/"
  }
});

lazyRequireTask("build-styles", "./gulp-tasks/build-styles.js", {
  src: [
    settings.app_dir + "/sass/main.sass",
    settings.app_dir + "/sass/components/header.sass"
  ],
  dest: settings.app_dir,
  newer: settings.app_dir,
  autoprefixer: settings.autoprefixer_options,
  prod: production
}, production);

lazyRequireTask("concat-js-libs", "./gulp-tasks/concat-js-libs.js", {
  src: settings.app_dir + "/js/libs/**/*.js",
  dest: settings.app_dir + "/js",
  newer: settings.app_dir + "/js",
  file_name: "libs.js"
});

lazyRequireTask("compress-js", "./gulp-tasks/compress-js.js", {
  src: settings.app_dir + "/js/*.js",
  dest: settings.build_dir + "/js",
  newer: settings.build_dir + "/js"
});

lazyRequireTask("compress-images", "./gulp-tasks/compress-images.js", {
  src: [
    settings.app_dir + "/img/**/*",
    "!" + settings.app_dir + "/img/**/*tmp*"
  ],
  dest: settings.build_dir + "/img",
  newer: settings.build_dir + "/img"
});

lazyRequireTask("create-png-sprite", "./gulp-tasks/create-png-sprite.js", {
  src: settings.app_dir + "/sprite/*.png",
  img_dest: settings.app_dir + "/img",
  css_dest: settings.app_dir + "/sass/modules",
  sprite_options: settings.sprite_options
});

lazyRequireTask("lint-js", "./gulp-tasks/lint-js.js", { src: settings.app_dir + "/js/common.js" });

lazyRequireTask("lint-css", "./gulp-tasks/lint-css.js", {
  src: [
    settings.app_dir + "/main.css",
    settings.app_dir + "/header.css"
  ],
  css_lint_config: settings.css_lint_config
});

lazyRequireTask("replace-styles", "./gulp-tasks/replace-styles.js", {
  src: settings.app_dir + "/index.html",
  grid_file: settings.app_dir + "/_grid.css",
  header_file: settings.app_dir + "/header.css",
  dest: settings.build_dir
});

lazyRequireTask("clean-build", "./gulp-tasks/clean-build.js", { src: settings.build_dir });

lazyRequireTask("copy-release-files", "./gulp-tasks/copy-release-files.js", {
  src: [
    settings.app_dir + "/**/*",
    settings.app_dir + "/.htaccess",
    "!" + settings.app_dir + "/index.html",
    "!" + settings.app_dir + "/_grid.css",
    "!" + settings.app_dir + "/header.css",
    "!" + settings.app_dir + "/jade{,/**}",
    "!" + settings.app_dir + "/sass{,/**}",
    "!" + settings.app_dir + "/img{,/**}",
    "!" + settings.app_dir + "/sprite{,/**}",
    "!" + settings.app_dir + "/js{,/**}",
    "!**/*.sass"
  ],
  dest: settings.build_dir
});


lazyRequireTask("browser-sync", "./gulp-tasks/browser-sync.js", {
  browser_sync_options: settings.browser_sync_server,
  watch_files: settings.app_dir + "/**/*.{html,css,js}"
});


gulp.task("set-production", function ( callback ) {
  production = true;
  callback();
});


gulp.task("watch", function () {
  gulp.watch(settings.app_dir + "/sass/**/*.sass", gulp.series("build-styles"));
  gulp.watch(settings.app_dir + "/jade/**/*.jade", gulp.series("build-html"));
  gulp.watch(settings.app_dir + "/js/libs/**/*.js", gulp.series("concat-js-libs"));
  gulp.watch(settings.app_dir + "/sprite/*.png", gulp.series("create-png-sprite"));
});



gulp.task("lint", gulp.parallel("lint-js", "lint-css"));

gulp.task("release", gulp.series("set-production", "clean-build", gulp.parallel("build-html", "build-styles", "concat-js-libs"), "copy-release-files", "create-png-sprite", gulp.parallel("compress-js", "compress-images"), "replace-styles"));

gulp.task("default", gulp.series("build-html", "build-styles", "concat-js-libs", "create-png-sprite", gulp.parallel("watch", "browser-sync")));
