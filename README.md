#Сборщик для верстки адаптивных LandingPage

Простой сборщик для верстки адаптивных LandingPage с использванием Jade и Sass основанный на Gulp 4. Шаблон содержит все необходимое для верстки и дальнейшей загрузки сайта на сервер.

##Компоненты используемые в сборщике:
  - Jade
  - Sass + Bourbon + Animate.css
  - Модульная сетка Bootstrap
  - Базовые JavaScript библиотеки: jQuery, html5shiv, modernizr, waypoints.js  
  - Скрипт для отправки данных форм с сайта на указанный email
