"use strict";

const gulp = require("gulp");
const uglify = require("gulp-uglify");
const newer = require("gulp-newer");

module.exports = function ( options ) {
  return function () {
    return gulp.src(options.src)
      .pipe(newer(options.newer))
      .pipe(uglify())
      .pipe(gulp.dest(options.dest));
  };
};
