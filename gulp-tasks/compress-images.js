"use strict";

const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const newer = require("gulp-newer");
const pngquant = require("imagemin-pngquant");

module.exports = function ( options ) {
  return function () {

    return gulp.src(options.src, { since: gulp.lastRun("compress-images") })
      .pipe(newer(options.newer))
      .pipe(imagemin({
        progressive: true,
        svgoPlugins: [
          {
            removeViewBox: false
          }
        ],
        use: [
          pngquant()
        ],
        interlaced: true
      }))
      .pipe(gulp.dest(options.dest));
  };
};
