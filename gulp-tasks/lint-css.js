"use strict";

const gulp = require("gulp");
const csslint = require("gulp-csslint");

var customReporter = function ( file ) {
  var count = 1;

  console.log("=====================");
  console.log(file.csslint.errorCount + " errors in " + file.relative);
  console.log("=====================");

  file.csslint.results.forEach(function ( result ) {
    console.log(count + ". " + result.error.message + " on line " + result.error.line);
    count++;
  });
};

module.exports = function ( options ) {
  return function () {
    return gulp.src(options.src)
      .pipe(csslint(options.css_lint_config))
      .pipe(csslint.reporter(customReporter));
  };
};
