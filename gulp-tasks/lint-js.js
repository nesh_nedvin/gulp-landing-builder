"use strict";

const gulp = require("gulp");
const jshint = require("gulp-jshint");
const stylish = require("jshint-stylish");

module.exports = function ( options ) {
  return function () {
    return gulp.src(options.src)
      .pipe(jshint())
      .pipe(jshint.reporter(stylish));
  };
};
