"use strict";

const gulp = require("gulp");
const concat = require("gulp-concat");
const notify = require("gulp-notify");
const plumber = require("gulp-plumber");
const newer = require("gulp-newer");

module.exports = function ( options ) {
  return function () {
    return gulp.src(options.src, { since: gulp.lastRun("concat-js-libs") })
      .pipe(newer(options.newer))
      .pipe(plumber({
        errorHandler: notify.onError(function ( error ) {
          return { title: "JavaScript", message: error.message };
        })
      }))
      .pipe(concat(options.file_name))
      .pipe(gulp.dest(options.dest));
  };
};
