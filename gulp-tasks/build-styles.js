"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const newer = require("gulp-newer");
const plumber = require("gulp-plumber");
const notify = require("gulp-notify");
const gulpif = require("gulp-if");
const sourcemaps = require("gulp-sourcemaps");
const cssnano = require("gulp-cssnano");

module.exports = function ( options, production ) {
  return function () {
    return gulp.src(options.src, { since: gulp.lastRun("build-styles") })
      .pipe(newer(options.newer))
      .pipe(plumber({
        errorHandler: notify.onError(function ( error ) {
          return { title: "Sass", message: error.message };
        })
      }))
      .pipe(gulpif(!production, sourcemaps.init()))
      .pipe(sass({ sourcemap: true, includePaths: require("node-bourbon").includePaths }))
      .pipe(autoprefixer(options.autoprefixer))
      .pipe(gulpif(!production, sourcemaps.write()))
      .pipe(gulpif(production, cssnano()))
      .pipe(gulp.dest(options.dest));
  };
};
