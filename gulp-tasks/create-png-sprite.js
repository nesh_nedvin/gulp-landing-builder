"use strict";

const gulp = require("gulp");
const spritesmith = require("gulp.spritesmith");
const merge = require("merge-stream");

module.exports = function ( options ) {
  return function () {
    var spriteData = gulp.src(options.src).pipe(spritesmith(options.sprite_options));
    var img = spriteData.img.pipe(gulp.dest(options.img_dest));
    var css = spriteData.css.pipe(gulp.dest(options.css_dest));

    return merge(img, css);
  };
};
