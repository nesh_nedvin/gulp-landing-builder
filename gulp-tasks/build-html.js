"use strict";

const gulp = require("gulp");
const jade = require("gulp-jade");
const jadeInheritance = require("gulp-jade-inheritance");
const newer = require("gulp-newer");
const plumber = require("gulp-plumber");
const notify = require("gulp-notify");

module.exports = function ( options ) {
  return function () {
    return gulp.src(options.src, { since: gulp.lastRun("build-html") })
      .pipe(newer(options.newer))
      .pipe(plumber({
        errorHandler: notify.onError(function ( error ) {
          return { title: "Jade", message: error.message };
        })
      }))
      .pipe(jadeInheritance(options.jade_inheritance))
      .pipe(jade(options.jade))
      .pipe(gulp.dest(options.dest));
  };
};
