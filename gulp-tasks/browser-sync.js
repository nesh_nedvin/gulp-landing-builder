"use strict";

const browserSync = require("browser-sync").create();

module.exports = function ( options ) {
  return function () {
    browserSync.init(options.browser_sync_options);
    browserSync.watch(options.watch_files).on("change", browserSync.reload);
  };
};
