"use strict";

const gulp = require("gulp");
const replace = require("gulp-replace");
const fs = require("fs");

module.exports = function ( options ) {
  return function () {
    return gulp.src(options.src)
      .pipe(replace(/<.*"_grid.css">/, function () {
        var style = fs.readFileSync(options.grid_file, "utf8");
        return "<style>" + style + "</style>";
      }))
      .pipe(replace(/<.*"header.css">/, function () {
        var style = fs.readFileSync(options.header_file, "utf8");
        return "<style>" + style + "</style>";
      }))
      .pipe(gulp.dest(options.dest));
  };
};
